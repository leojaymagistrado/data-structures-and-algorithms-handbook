# Aspiring Software Engineer

## Christian Visaya

👋 Aspiring Software Engineer! 🚀👨‍💻 — 💌 christianvisaya@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_visaya_christian.jpg](images/bsis_2_visaya_christian.jpg)

### Bio

**Good to know:** I'm a fan of optimization. One thing for sure I've learned throughout my career is that communication and understanding is the most significant part of solving a problem. I prefer clarity over speed.

**Motto:** Stability under pressure

**Languages:** Python, Javascript, PHP

**Other Technologies:** AWS, GCP, Microsoft Azure, Digital Ocean, Alibaba Cloud

**Personality Type:** [Protagonist (ENFJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->

# Aspiring Game Developer

## Leo Jay Magistrado

👋 Aspiring Web Developer! 🚀👨‍💻 — 💌 leojaymagistrado@laverdad.edu.ph — Apalit, Pampanga

![alt act_2_magistrado_leojay.jpg](images/act_2_magistrad_leojay.jpg)

### Bio

**Good to know:** I'm a fan of anime 

**Motto:** Time is Gold

**Languages:** Python, Java

**Other Technologies:** 

**Personality Type:** [Logistician ISTJ-A)](https://www.16personalities.com/profiles/c5ea44cd4a89a)

<!-- END -->
